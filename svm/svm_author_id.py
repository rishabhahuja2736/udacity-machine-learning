#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 2 (SVM) mini-project.

    Use a SVM to identify emails from the Enron corpus by their authors:    
    Sara has label 0
    Chris has label 1
"""
    
import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess
from sklearn import svm
from sklearn.metrics import accuracy_score

### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()
#print(type(features_train))
#########################################################

### your code goes here ###
def SVMAccuracy(features_train,labels_train,features_test,labels_test):
	clf=svm.SVC(C=10000.0, cache_size=200, class_weight=None, coef0=0.0,
    		decision_function_shape=None, degree=3, gamma='auto', kernel='rbf',
    		max_iter=-1, probability=False, random_state=None, shrinking=True,
    		tol=0.001, verbose=False)
	
	#features_train = features_train[:len(features_train)/100] 
	#labels_train = labels_train[:len(labels_train)/100] 
	
	t0=time()
	clf.fit(features_train,labels_train)
	print "classify training time:", round(time()-t0, 3), "s"

	t0=time()
	pred = clf.predict(features_test)
	#answer= pred[10]
	#answer= pred[26]
	#answer= pred[50]
	#print answer
	count=0
	for i in pred:
		if i==1:
			count=count+1
	print count	
	print "classify training time:", round(time()-t0, 3), "s"
	accuracy = accuracy_score(labels_test,pred)
	print accuracy
#########################################################
SVMAccuracy(features_train,labels_train,features_test,labels_test)

